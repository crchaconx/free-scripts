#!/bin/bash
#Add execution permission to the script  chmod +x splitpdf.sh 
#Dependencies: pdftk
#Takes a input and splits a new PDF document every {interval} pages.
#
pagestart=1
interval=3
for i in {1..6}
do
  pdftk $1 cat  $pagestart $((pagestart+1)) $((pagestart+2)) output 0$i$1
  pagestart=$((pagestart + interval)) 
done
